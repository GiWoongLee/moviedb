package newMovieDB;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Genre, Title 을 관리하는 영화 데이터베이스.
 * 
 * MyLinkedList 를 사용해 각각 Genre와 Title에 따라 내부적으로 정렬된 상태를  
 * 유지하는 데이터베이스이다. 
 */
public class MovieDB {
	MyLinkedList<Genre> genreList;
	
    public MovieDB() {
    	genreList = new MyLinkedList<Genre>();
    }

    public void insert(MovieDBItem item) {
    	//STEP1 : 기존 List 내 Genre/MovieDBItem이 존재하는 지 확인
    	//STEP2 : 상황에 따라 적절하게 Genre/MovieDBItem insert
    	
    	Genre target = new Genre(item.getGenre());
    	
    	if(searchGenre(target)==null){
    		insertGenre(target);
    		Genre targetGenre = searchGenre(target).getItem();
    		insertMovie(targetGenre,item);
    	}
    	
    	else if(searchGenre(target)!=null&&searchMovie(searchGenre(target).getItem(),item)==null){
    		Genre targetGenre = searchGenre(target).getItem();
    		insertMovie(targetGenre,item);   	
    	}
		else{
			return;
    	}
    }

    //search 대상의 Genre가 위치한 Node return
    private Node<Genre> searchGenre(Genre target){
    	
    	Node<Genre> pointer = genreList.head;
    	
    	if(pointer.getNext()==null)
    		return null;
    	
    	while(pointer.getNext()!=null){
    		if(pointer.getNext().getItem().equals(target))
				return pointer.getNext();
			pointer = pointer.getNext();
		}
		
    	return null;
    }
    
    //찾고자 하는 Node의 prev Node return. prev Node를 return하는 건,
    //Insert 시 insertNext(),Delete 시 removeNext()가 용이하도록 하기 위함  
    private Node<MovieDBItem> searchMovie(Genre targetGenre,MovieDBItem targetMovie){
    	
    	Node<MovieDBItem> pointer = targetGenre.movieList.head;
    	
    	if(pointer.getNext()==null){
    		return null;
    	}
    	
    	else{
    		//CASE1 : 두번째~마지막 Node까지 일치여부 비교
    		while(pointer.getNext()!=null){
    			if(pointer.getNext().getItem().equals(targetMovie)){
    				return pointer;
    			}
    			pointer=pointer.getNext();
    		}
    		
    		//CASE2 : 마지막 Node까지 해당 Node 부재
    		if(pointer.getNext()==null)
    			return null;
    	}
    	
    	return null;
    }
    
   
    private void insertGenre(Genre newGenre){
    	Node<Genre> pointer = genreList.head;
    	
    	if(pointer.getNext()==null){
    		pointer.insertNext(newGenre);
    	}
    	
    	else{
    		//CASE1: 두번째 Node부터 마지막 Node의 장르와 비교 후 insert
    		while(pointer.getNext()!=null){
    			if(pointer.getNext().getItem().compareTo(newGenre)>0){
    				pointer.insertNext(newGenre);
    				return;
    			}
    			pointer=pointer.getNext(); 
    		}
    		
    		//CASE2: 마지막 Node의 장르보다도 순서가 늦을 경우
    		if(pointer.getNext()==null)
    			pointer.insertNext(newGenre);
    	}
    }
    
    private void insertMovie(Genre targetGenre, MovieDBItem item){
    	Node<MovieDBItem> pointer = targetGenre.movieList.head;
    	
    	//empty list
    	if(pointer.getNext()==null){
    		pointer.insertNext(item);
    		return;
    	}
    	
    	else{
    		//CASE1: 첫번째 Node부터 마지막 Node까지 title이 들어갈 위치 탐색, insert
    		while(pointer.getNext()!=null){
    			if(pointer.getNext().getItem().compareTo(item)>0){
    				pointer.insertNext(item);
    				return;
    			}
    			pointer = pointer.getNext();
    		}
    		//CASE2 : 마지막 Node의 title보다 순서가 뒤에 위치할 때
    		if(pointer.getNext()==null){
    			pointer.insertNext(item);
    			return;
    		}
    	}
    }
    
    
    public void delete(MovieDBItem item) {
        Genre target = new Genre(item.getGenre());
    	Genre targetGenre = searchGenre(target).getItem();
    	if(targetGenre==null || searchMovie(targetGenre,item)==null)
    		return;
    	else{
    		//삭제할 Node의 prevNode를 받아서 remove
    		Node<MovieDBItem> position = searchMovie(targetGenre,item);
    		position.removeNext();
    	}
    }
    
    
    public MyLinkedList<MovieDBItem> search(String term) {
    	CharSequence s =term;
    	MyLinkedList<MovieDBItem> results = new MyLinkedList<MovieDBItem>();
        Node<Genre> pointer = genreList.head;
        if(pointer.getNext()==null)
        	return null;
        else{
        	//Genre와 각 Genre별 Title을 연쇄해서 탐색
        	while(pointer.getNext()!=null){
        		Genre target = pointer.getNext().getItem();
        		Node<MovieDBItem> finder = target.movieList.head;
        		while(finder.getNext()!=null){
        			MovieDBItem targetItem = finder.getNext().getItem();
        			if(targetItem.getTitle().contains(s)){
        				results.add(targetItem);
        			}
        			finder = finder.getNext();
        		}
        		pointer = pointer.getNext();
        	}
        	return results;
        }
    }
    
    public MyLinkedList<MovieDBItem> items() {
      MyLinkedList<MovieDBItem> results = new MyLinkedList<MovieDBItem>();
		Node<Genre> pointer = genreList.head;
		while(pointer.getNext()!=null){
			Node<MovieDBItem> finder = pointer.getNext().getItem().movieList.head;
			while(finder.getNext()!=null){
				MovieDBItem item = finder.getNext().getItem();
				results.add(item);
				finder = finder.getNext();
			}
			pointer = pointer.getNext();
		}
    	return results;
    }
}

class Genre extends Node<String> implements Comparable<Genre> {
	MyLinkedList<MovieDBItem> movieList;
	public Genre(String name) {
		super(name);
		movieList = new MyLinkedList<MovieDBItem>();
	}
	
	@Override
	//다른 Genre와 제목을 비교할 때, 'z'에 더 가깝다면 '1' 출력 
	public int compareTo(Genre o) {
		if(this.getItem().compareTo(o.getItem())>0)
			return 1;
		else if(this.getItem().compareTo(o.getItem())==0)
			return 0;
		else if(this.getItem().compareTo(o.getItem())<0)
			return -1;
		else
			throw new NoSuchElementException();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getItem() == null) ? 0 : this.getItem().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (! (obj instanceof Genre)) return false;
		Genre compareTarget = (Genre) obj;
		if(this.getItem()==null){
			if(compareTarget.getItem()!=null)
				return false;
		} 
		else if(!this.getItem().equals(compareTarget.getItem()))
			return false;
		return true;
	}
}

class MovieList implements ListInterface<String> {
	public MovieList() {
		
	}

	@Override
	public Iterator<String> iterator() {
		
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public boolean isEmpty() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public int size() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public void add(String item) {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public String first() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public void removeAll() {
		throw new UnsupportedOperationException("not implemented yet");
	}
}